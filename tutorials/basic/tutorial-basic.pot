msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-06-17 21:50+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: basic-f06.svg:44(format) basic-f12.svg:44(format) basic-f16.svg:44(format) basic-f04.svg:44(format) basic-f15.svg:44(format) basic-f07.svg:44(format) basic-f03.svg:44(format) basic-f14.svg:44(format) basic-f13.svg:212(format) basic-f01.svg:44(format) basic-f05.svg:44(format) basic-f11.svg:228(format) basic-f08.svg:44(format) basic-f10.svg:44(format) basic-f02.svg:44(format) basic-f09.svg:44(format)
msgid "image/svg+xml"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:100(None)
msgid "@@image: 'basic-f01.svg'; md5=b3c4f1a675543eee4da6bb7c993a3c7b"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:111(None)
msgid "@@image: 'basic-f02.svg'; md5=730230c3d3e76194edbecb4640835c6e"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:136(None)
msgid "@@image: 'basic-f03.svg'; md5=d2c90ff1d8ef14afc95b9211109a72e8"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:217(None)
msgid "@@image: 'basic-f04.svg'; md5=abcdf89351467afdd29f79e25ce6a83c"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:227(None)
msgid "@@image: 'basic-f05.svg'; md5=eb374da4026e3db73a557b1fe45dddbf"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:257(None)
msgid "@@image: 'basic-f06.svg'; md5=f4a970df11886ce66f21b6a4ee36ccf4"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:296(None)
msgid "@@image: 'basic-f07.svg'; md5=38b60d10f9a0946a992de672bbdc0cd6"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:319(None)
msgid "@@image: 'basic-f08.svg'; md5=06825e44e3b8b7244466bc089623b4c9"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:329(None)
msgid "@@image: 'basic-f09.svg'; md5=4b67f9da48b59c74d3bd23c8cda8bcf6"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:339(None)
msgid "@@image: 'basic-f10.svg'; md5=4e87363ff71f6cbb7d9a8782c865c011"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:349(None)
msgid "@@image: 'basic-f11.svg'; md5=08a64300c70200d0ffc1811d16fda65f"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:378(None)
msgid "@@image: 'basic-f12.svg'; md5=eb08c965826208aeb162a2de040bb60f"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:393(None)
msgid "@@image: 'basic-f13.svg'; md5=7bc000ebb72d9737d46d7270a4f3294a"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:418(None)
msgid "@@image: 'basic-f14.svg'; md5=6ea204091566c694cbcdf6dc1b1926bb"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:471(None)
msgid "@@image: 'basic-f15.svg'; md5=46f334c0b9cf0b6841c499184429e1c0"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-basic.xml:488(None)
msgid "@@image: 'basic-f16.svg'; md5=84e95548f96dd4b3c584d0e30ada8aa2"
msgstr ""

#: tutorial-basic.xml:4(title)
msgid "Basic"
msgstr ""

#: tutorial-basic.xml:5(author)
msgid "bulia byak, buliabyak@users.sf.net"
msgstr ""

#: tutorial-basic.xml:9(para)
msgid "This tutorial demonstrates the basics of using Inkscape. This is a regular Inkscape document that you can view, edit, copy from, or save."
msgstr ""

#: tutorial-basic.xml:13(para)
msgid "The Basic Tutorial covers canvas navigation, managing documents, shape tool basics, selection techniques, transforming objects with selector, grouping, setting fill and stroke, alignment, and z-order. For more advanced topics, check out the other tutorials in the Help menu."
msgstr ""

#: tutorial-basic.xml:22(title)
msgid "Panning the canvas"
msgstr ""

#: tutorial-basic.xml:24(para)
msgid "There are many ways to pan (scroll) the document canvas. Try <keycap>Ctrl+arrow</keycap> keys to scroll by keyboard. (Try this now to scroll this document down.) You can also drag the canvas by the middle mouse button. Or, you can use the scrollbars (press <keycap>Ctrl+B</keycap> to show or hide them). The <keycap>wheel</keycap> on your mouse also works for scrolling vertically; press <keycap>Shift</keycap> with the wheel to scroll horizontally."
msgstr ""

#: tutorial-basic.xml:33(title)
msgid "Zooming in or out"
msgstr ""

#: tutorial-basic.xml:34(para)
msgid "The easiest way to zoom is by pressing <keycap>-</keycap> and <keycap>+</keycap> (or <keycap>=</keycap>) keys. You can also use <keycap>Ctrl+middle click</keycap> or <keycap>Ctrl+right click</keycap> to zoom in, <keycap>Shift+middle click</keycap> or <keycap>Shift+right click</keycap> to zoom out, or rotate the mouse wheel with <keycap>Ctrl</keycap>. Or, you can click in the zoom entry field (in the bottom right corner of the document window), type a precise zoom value in %, and press Enter. We also have the Zoom tool (in the toolbar on left) which lets you to zoom into an area by dragging around it."
msgstr ""

#: tutorial-basic.xml:43(para)
msgid "Inkscape also keeps a history of the zoom levels you've used in this work session. Press the <keycap>`</keycap> key to go back to the previous zoom, or <keycap>Shift+`</keycap> to go forward."
msgstr ""

#: tutorial-basic.xml:50(title)
msgid "Inkscape tools"
msgstr ""

#: tutorial-basic.xml:52(para)
msgid "The vertical toolbar on the left shows Inkscape's drawing and editing tools. In the top part of the window, below the menu, there's the <firstterm>Commands bar</firstterm> with general command buttons and the <firstterm>Tool Controls bar</firstterm> with controls that are specific to each tool. The <firstterm>status bar</firstterm> at the bottom of the window will display useful hints and messages as you work."
msgstr ""

#: tutorial-basic.xml:59(para)
msgid "Many operations are available through keyboard shortcuts. Open Help &gt; Keys and Mouse to see the complete reference."
msgstr ""

#: tutorial-basic.xml:64(title)
msgid "Creating and managing documents"
msgstr ""

#: tutorial-basic.xml:66(para)
msgid "To create a new empty document, use <command>File &gt; New</command> or press <keycap>Ctrl+N</keycap>. To create a new document from one of Inkscape's many templates, use <command>File &gt; New from Template...</command> or press <keycap>Ctrl+Alt+N</keycap>"
msgstr ""

#: tutorial-basic.xml:71(para)
msgid "To open an existing SVG document, use <command>File &gt; Open</command> (<keycap>Ctrl+O</keycap>). To save, use <command>File &gt; Save</command> (<keycap>Ctrl+S</keycap>), or <command>Save As</command> (<keycap>Shift+Ctrl+S</keycap>) to save under a new name. (Inkscape may still be unstable, so remember to save often!)"
msgstr ""

#: tutorial-basic.xml:77(para)
msgid "Inkscape uses the SVG (Scalable Vector Graphics) format for its files. SVG is an open standard widely supported by graphic software. SVG files are based on XML and can be edited with any text or XML editor (apart from Inkscape, that is). Besides SVG, Inkscape can import and export several other formats (EPS, PNG)."
msgstr ""

#: tutorial-basic.xml:82(para)
msgid "Inkscape opens a separate document window for each document. You can navigate among them using your window manager (e.g. by <keycap>Alt+Tab</keycap>), or you can use the Inkscape shortcut, <keycap>Ctrl+Tab</keycap>, which will cycle through all open document windows. (Create a new document now and switch between it and this document for practice.) Note: Inkscape treats these windows like tabs in a web browser, this means the <keycap>Ctrl+Tab</keycap> shortcut only works with documents running in the same process. If you open multiple files from a file browser or launch more than one Inkscape process from an icon it will not work."
msgstr ""

#: tutorial-basic.xml:93(title)
msgid "Creating shapes"
msgstr ""

#: tutorial-basic.xml:94(para)
msgid "Time for some nice shapes! Click on the Rectangle tool in the toolbar (or press <keycap>F4</keycap>) and click-and-drag, either in a new empty document or right here:"
msgstr ""

#: tutorial-basic.xml:105(para)
msgid "As you can see, default rectangles come up blue, with a black <firstterm>stroke</firstterm> (outline), and fully opaque. We'll see how to change that below. With other tools, you can also create ellipses, stars, and spirals:"
msgstr ""

#: tutorial-basic.xml:116(para)
msgid "These tools are collectively known as <firstterm>shape tools</firstterm>. Each shape you create displays one or more diamond-shaped <firstterm>handles</firstterm>; try dragging them to see how the shape responds. The Controls panel for a shape tool is another way to tweak a shape; these controls affect the currently selected shapes (i.e. those that display the handles) <emphasis>and</emphasis> set the default that will apply to newly created shapes."
msgstr ""

#: tutorial-basic.xml:122(para)
msgid "To <firstterm>undo</firstterm> your last action, press <keycap>Ctrl+Z</keycap>. (Or, if you change your mind again, you can <firstterm>redo</firstterm> the undone action by <keycap>Shift+Ctrl+Z</keycap>.)"
msgstr ""

#: tutorial-basic.xml:129(title)
msgid "Moving, scaling, rotating"
msgstr ""

#: tutorial-basic.xml:129(para)
msgid "The most frequently used Inkscape tool is the <firstterm>Selector</firstterm>. Click the topmost button (with the arrow) on the toolbar, or press <keycap>F1</keycap> or <keycap>Space</keycap>. Now you can select any object on the canvas. Click on the rectangle below."
msgstr ""

#: tutorial-basic.xml:141(para)
msgid "You will see eight arrow-shaped handles appear around the object. Now you can:"
msgstr ""

#: tutorial-basic.xml:145(para)
msgid "<firstterm>Move</firstterm> the object by dragging it. (Press <keycap>Ctrl</keycap> to restrict movement to horizontal and vertical.)"
msgstr ""

#: tutorial-basic.xml:149(para)
msgid "<firstterm>Scale</firstterm> the object by dragging any handle. (Press <keycap>Ctrl</keycap> to preserve the original height/width ratio.)"
msgstr ""

#: tutorial-basic.xml:154(para)
msgid "Now click the rectangle again. The handles change. Now you can:"
msgstr ""

#: tutorial-basic.xml:158(para)
msgid "<firstterm>Rotate</firstterm> the object by dragging corner handles. (Press <keycap>Ctrl</keycap> to restrict rotation to 15 degree steps. Drag the cross mark to position the center of rotation.)"
msgstr ""

#: tutorial-basic.xml:163(para)
msgid "<firstterm>Skew</firstterm> (shear) the object by dragging non-corner handles. (Press <keycap>Ctrl</keycap> to restrict skewing to 15 degree steps.)"
msgstr ""

#: tutorial-basic.xml:168(para)
msgid "While in Selector, you can also use the numeric entry fields in the Controls bar (above the canvas) to set exact values for coordinates (X and Y) and size (W and H) of the selection."
msgstr ""

#: tutorial-basic.xml:174(title)
msgid "Transforming by keys"
msgstr ""

#: tutorial-basic.xml:176(para)
msgid "One of Inkscape's features that set it apart from most other vector editors is its emphasis on keyboard accessibility. There's hardly any command or action that is impossible to do from keyboard, and transforming objects is no exception."
msgstr ""

#: tutorial-basic.xml:180(para)
msgid "You can use the keyboard to move (<keycap>arrow</keycap> keys), scale (<keycap>&lt;</keycap> and <keycap>&gt;</keycap> keys), and rotate (<keycap>[</keycap> and <keycap>]</keycap> keys) objects. Default moves and scales are by 2 px; with <keycap>Shift</keycap>, you move by 10 times that. <keycap>Ctrl+&gt;</keycap> and <keycap>Ctrl+&lt;</keycap> scale up or down to 200% or 50% of the original, respectively. Default rotates are by 15 degrees; with <keycap>Ctrl</keycap>, you rotate by 90 degrees."
msgstr ""

#: tutorial-basic.xml:188(para)
msgid "However, perhaps the most useful are <firstterm>pixel-size transformations</firstterm>, invoked by using <keycap>Alt</keycap> with the transform keys. For example, <keycap>Alt+arrows</keycap> will move the selection by 1 pixel <emphasis>at the current zoom</emphasis> (i.e. by 1 <firstterm>screen pixel</firstterm>, not to be confused with the px unit which is an SVG length unit independent of zoom). This means that if you zoom in, one <keycap>Alt+arrow</keycap> will result in a <emphasis>smaller</emphasis> absolute movement which will still look like one-pixel nudge on your screen. It is thus possible to position objects with arbitrary precision simply by zooming in or out as needed."
msgstr ""

#: tutorial-basic.xml:198(para)
msgid "Similarly, <keycap>Alt+&gt;</keycap> and <keycap>Alt+&lt;</keycap> scale selection so that its visible size changes by one screen pixel, and <keycap>Alt+[</keycap> and <keycap>Alt+]</keycap> rotate it so that its farthest-from-center point moves by one screen pixel."
msgstr ""

#: tutorial-basic.xml:203(para)
msgid "Note: Linux users may not get the expected results with the <keycap>Alt+arrow</keycap> and a few other key combinations if their Window Manager catches those key events before they reach the inkscape application. One solution would be to change the WM's configuration accordingly."
msgstr ""

#: tutorial-basic.xml:207(title)
msgid "Multiple selections"
msgstr ""

#: tutorial-basic.xml:207(para)
msgid "You can select any number of objects simultaneously by <keycap>Shift+click</keycap>ing them. Or, you can <keycap>drag</keycap> around the objects you need to select; this is called <firstterm>rubberband selection</firstterm>. (Selector creates rubberband when dragging from an empty space; however, if you press <keycap>Shift</keycap> before starting to drag, Inkscape will always create the rubberband.) Practice by selecting all three of the shapes below:"
msgstr ""

#: tutorial-basic.xml:222(para)
msgid "Now, use rubberband (by drag or <keycap>Shift+drag</keycap>) to select the two ellipses but not the rectangle:"
msgstr ""

#: tutorial-basic.xml:232(para)
msgid "Each individual object within a selection displays a <firstterm>selection cue</firstterm> — by default, a dashed rectangular frame. These cues make it easy to see at once what is selected and what is not. For example, if you select both the two ellipses and the rectangle, without the cues you would have hard time guessing whether the ellipses are selected or not."
msgstr ""

#: tutorial-basic.xml:238(para)
msgid "<keycap>Shift+click</keycap>ing on a selected object excludes it from the selection. Select all three objects above, then use <keycap>Shift+click</keycap> to exclude both ellipses from the selection leaving only the rectangle selected."
msgstr ""

#: tutorial-basic.xml:242(para)
msgid "Pressing <keycap>Esc</keycap> deselects any selected objects. <keycap>Ctrl+A</keycap> selects all objects in the current layer (if you did not create layers, this is the same as all objects in the document)."
msgstr ""

#: tutorial-basic.xml:248(title)
msgid "Grouping"
msgstr ""

#: tutorial-basic.xml:250(para)
msgid "Several objects can be combined into a <firstterm>group</firstterm>. A group behaves as a single object when you drag or transform it. Below, the three objects on the left are independent; the same three objects on the right are grouped. Try to drag the group."
msgstr ""

#: tutorial-basic.xml:262(para)
msgid "To create a group, you select one or more objects and press <keycap>Ctrl+G</keycap>. To ungroup one or more groups, select them and press <keycap>Ctrl+U</keycap>. Groups themselves may be grouped, just like any other objects; such recursive groups may go down to arbitrary depth. However, <keycap>Ctrl+U</keycap> only ungroups the topmost level of grouping in a selection; you'll need to press <keycap>Ctrl+U</keycap> repeatedly if you want to completely ungroup a deep group-in-group."
msgstr ""

#: tutorial-basic.xml:270(para)
msgid "You don't necessarily have to ungroup, however, if you want to edit an object within a group. Just <keycap>Ctrl+click</keycap> that object and it will be selected and editable alone, or <keycap>Shift+Ctrl+click</keycap> several objects (inside or outside any groups) for multiple selection regardless of grouping. Try to move or transform the individual shapes in the group (above right) without ungrouping it, then deselect and select the group normally to see that it still remains grouped."
msgstr ""

#: tutorial-basic.xml:280(title)
msgid "Fill and stroke"
msgstr ""

#: tutorial-basic.xml:282(para)
msgid "Probably the simplest way to paint an object some color is to select an object, and click a swatch in the palette below the canvas to paint it (change its fill color). Alternatively, you can open the Swatches dialog from the View menu (or press <keycap>Shift+Ctrl+W</keycap>), select an object, and click a swatch to paint it (change its fill color)."
msgstr ""

#: tutorial-basic.xml:290(para)
msgid "More powerful is the Fill and Stroke dialog from the <guimenu>Object</guimenu> menu (or press <keycap>Shift+Ctrl+F</keycap>). Select the shape below and open the Fill and Stroke dialog."
msgstr ""

#: tutorial-basic.xml:301(para)
msgid "You will see that the dialog has three tabs: Fill, Stroke paint, and Stroke style. The Fill tab lets you edit the <firstterm>fill</firstterm> (interior) of the selected object(s). Using the buttons just below the tab, you can select types of fill, including no fill (the button with the X), flat color fill, as well as linear or radial gradients. For the above shape, the flat fill button will be activated."
msgstr ""

#: tutorial-basic.xml:307(para)
msgid "Further below, you see a collection of <firstterm>color pickers</firstterm>, each in its own tab: RGB, CMYK, HSL, and Wheel. Perhaps the most convenient is the Wheel picker, where you can rotate the triangle to choose a hue on the wheel, and then select a shade of that hue within the triangle. All color pickers contain a slider to set the <firstterm>alpha</firstterm> (opacity) of the selected object(s)."
msgstr ""

#: tutorial-basic.xml:313(para)
msgid "Whenever you select an object, the color picker is updated to display its current fill and stroke (for multiple selected objects, the dialog shows their <emphasis>average</emphasis> color). Play with these samples or create your own:"
msgstr ""

#: tutorial-basic.xml:324(para)
msgid "Using the Stroke paint tab, you can remove the <firstterm>stroke</firstterm> (outline) of the object, or assign any color or transparency to it:"
msgstr ""

#: tutorial-basic.xml:334(para)
msgid "The last tab, Stroke style, lets you set the width and other parameters of the stroke:"
msgstr ""

#: tutorial-basic.xml:344(para)
msgid "Finally, instead of flat color, you can use <firstterm>gradients</firstterm> for fills and/or strokes:"
msgstr ""

#: tutorial-basic.xml:354(para)
msgid "When you switch from flat color to gradient, the newly created gradient uses the previous flat color, going from opaque to transparent. Switch to the Gradient tool (<keycap>Ctrl+F1</keycap>) to drag the <firstterm>gradient handles</firstterm> — the controls connected by lines that define the direction and length of the gradient. When any of the gradient handles is selected (highlighted blue), the Fill and Stroke dialog sets the color of that handle instead of the color of the entire selected object."
msgstr ""

#: tutorial-basic.xml:362(para)
msgid "Yet another convenient way to change a color of an object is by using the Dropper tool (<keycap>F7</keycap>). Just <keycap>click</keycap> anywhere in the drawing with that tool, and the picked color will be assigned to the selected object's fill (<keycap>Shift+click</keycap> will assign stroke color)."
msgstr ""

#: tutorial-basic.xml:369(title)
msgid "Duplication, alignment, distribution"
msgstr ""

#: tutorial-basic.xml:371(para)
msgid "One of the most common operations is <firstterm>duplicating</firstterm> an object (<keycap>Ctrl+D</keycap>). The duplicate is placed exactly above the original and is selected, so you can drag it away by mouse or by arrow keys. For practice, try to fill the line with copies of this black square:"
msgstr ""

#: tutorial-basic.xml:383(para)
msgid "Chances are, your copies of the square are placed more or less randomly. This is where the Align and Distribute dialog (<keycap>Shift+Ctrl+A</keycap>) is useful. Select all the squares (<keycap>Shift+click</keycap> or drag a rubberband), open the dialog and press the “Center on horizontal axis” button, then the “Make horizontal gaps between objects equal” button (read the button tooltips). The objects are now neatly aligned and distributed equispacedly. Here are some other alignment and distribution examples:"
msgstr ""

#: tutorial-basic.xml:401(title)
msgid "Z-order"
msgstr ""

#: tutorial-basic.xml:403(para)
msgid "The term <firstterm>z-order</firstterm> refers to the stacking order of objects in a drawing, i.e. to which objects are on top and obscure others. The two commands in the Object menu, Raise to Top (the <keycap>Home</keycap> key) and Lower to Bottom (the <keycap>End</keycap> key), will move your selected objects to the very top or very bottom of the current layer's z-order. Two more commands, Raise (<keycap>PgUp</keycap>) and Lower (<keycap>PgDn</keycap>), will sink or emerge the selection <emphasis>one step only</emphasis>, i.e. move it past one non-selected object in z-order (only objects that overlap the selection count, based on their respective bounding boxes)."
msgstr ""

#: tutorial-basic.xml:412(para)
msgid "Practice using these commands by reversing the z-order of the objects below, so that the leftmost ellipse is on top and the rightmost one is at the bottom:"
msgstr ""

#: tutorial-basic.xml:423(para)
msgid "A very useful selection shortcut is the <keycap>Tab</keycap> key. If nothing is selected, it selects the bottommost object; otherwise it selects the object <emphasis>above the selected object(s)</emphasis> in z-order. <keycap>Shift+Tab</keycap> works in reverse, starting from the topmost object and proceeding downwards. Since the objects you create are added to the top of the stack, pressing <keycap>Shift+Tab</keycap> with nothing selected will conveniently select the object you created <emphasis>last</emphasis>. Practice the <keycap>Tab</keycap> and <keycap>Shift+Tab</keycap> keys on the stack of ellipses above."
msgstr ""

#: tutorial-basic.xml:434(title)
msgid "Selecting under and dragging selected"
msgstr ""

#: tutorial-basic.xml:436(para)
msgid "What to do if the object you need is hidden behind another object? You may still see the bottom object if the top one is (partially) transparent, but clicking on it will select the top object, not the one you need."
msgstr ""

#: tutorial-basic.xml:441(para)
msgid "This is what <keycap>Alt+click</keycap> is for. First <keycap>Alt+click</keycap> selects the top object just like the regular click. However, the next <keycap>Alt+click</keycap> at the same point will select the object <emphasis>below</emphasis> the top one; the next one, the object still lower, etc. Thus, several <keycap>Alt+click</keycap>s in a row will cycle, top-to-bottom, through the entire z-order stack of objects at the click point. When the bottom object is reached, next <keycap>Alt+click</keycap> will, naturally, again select the topmost object."
msgstr ""

#: tutorial-basic.xml:449(para)
msgid "[If you are on Linux, you might find that <keycap>Alt+click</keycap> does not work properly. Instead, it might be moving the whole Inkscape window. This is because your window manager has reserved Alt+click for a different action. The way to fix this is to find the Window Behavior configuration for your window manager, and either turn it off, or map it to use the Meta key (aka Windows key), so Inkscape and other applications may use the Alt key freely.]"
msgstr ""

#: tutorial-basic.xml:458(para)
msgid "This is nice, but once you selected an under-the-surface object, what can you do with it? You can use keys to transform it, and you can drag the selection handles. However, dragging the object itself will reset the selection to the top object again (this is how click-and-drag is designed to work — it selects the (top) object under cursor first, then drags the selection). To tell Inkscape to drag <emphasis>what is selected now</emphasis> without selecting anything else, use <keycap>Alt+drag</keycap>. This will move the current selection no matter where you drag your mouse."
msgstr ""

#: tutorial-basic.xml:466(para)
msgid "Practice <keycap>Alt+click</keycap> and <keycap>Alt+drag</keycap> on the two brown shapes under the green transparent rectangle:"
msgstr ""

#: tutorial-basic.xml:479(title)
msgid "Selecting similar objects"
msgstr ""

#: tutorial-basic.xml:480(para)
msgid "Inkscape can select other objects similar to the object currently selected. For example, if you want to select all the blue squares below first select one of the blue squares, and use <command>Edit &gt; Select Same &gt; Fill Color</command> from the menu. All the objects with a fill color the same shade of blue are now selected."
msgstr ""

#: tutorial-basic.xml:493(para)
msgid "In addition to selecting by fill color, you can select multiple similar objects by stroke color, stroke style, fill &amp; stroke, and object type."
msgstr ""

#: tutorial-basic.xml:499(title)
msgid "Conclusion"
msgstr ""

#: tutorial-basic.xml:501(para)
msgid "This concludes the Basic tutorial. There's much more than that to Inkscape, but with the techniques described here, you will already be able to create simple yet useful graphics. For more complicated stuff, go through the Advanced and other tutorials in <command>Help &gt; Tutorials</command>."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-basic.xml:0(None)
msgid "translator-credits"
msgstr ""

