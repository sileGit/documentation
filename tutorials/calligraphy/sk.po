# translation of Inkscape tutorial calligraphy to Slovak
# Copyright (C)
# This file is distributed under the same license as the Inkscape package.
# Peter Mráz <etkinator@gmail.com>, 2009.
# Ivan Masár <helix84@centrum.sk>, 2009, 2010, 2015.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2019-06-17 21:51+0200\n"
"PO-Revision-Date: 2015-01-30 15:40+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: calligraphy-f10.svg:45(format) calligraphy-f09.svg:44(format)
#: calligraphy-f08.svg:44(format) calligraphy-f07.svg:42(format)
#: calligraphy-f06.svg:59(format) calligraphy-f05.svg:45(format)
#: calligraphy-f04.svg:74(format) calligraphy-f03.svg:44(format)
#: calligraphy-f02.svg:45(format) calligraphy-f01.svg:45(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: calligraphy-f10.svg:107(tspan)
#, no-wrap
msgid "Uncial hand"
msgstr "rukopis Unicial"

#: calligraphy-f10.svg:119(tspan)
#, no-wrap
msgid "Carolingian hand"
msgstr "rukopis Carolingian"

#: calligraphy-f10.svg:131(tspan)
#, no-wrap
msgid "Gothic hand"
msgstr "rukopis Gothic"

#: calligraphy-f10.svg:143(tspan)
#, no-wrap
msgid "Bâtarde hand"
msgstr "rukopis Bâtarde"

#: calligraphy-f10.svg:167(tspan)
#, no-wrap
msgid "Flourished Italic hand"
msgstr "rukopis Flourished Italic"

#: calligraphy-f07.svg:65(tspan)
#, no-wrap
msgid "slow"
msgstr "pomaly"

#: calligraphy-f07.svg:75(tspan)
#, no-wrap
msgid "medium"
msgstr "stredne"

#: calligraphy-f07.svg:85(tspan)
#, no-wrap
msgid "fast"
msgstr "rýchlo"

#: calligraphy-f07.svg:169(tspan)
#, no-wrap
msgid "tremor = 0"
msgstr "chvenie = 0"

#: calligraphy-f07.svg:180(tspan)
#, no-wrap
msgid "tremor = 10"
msgstr "chvenie = 10"

#: calligraphy-f07.svg:191(tspan)
#, no-wrap
msgid "tremor = 30"
msgstr "chvenie = 30"

#: calligraphy-f07.svg:202(tspan)
#, no-wrap
msgid "tremor = 50"
msgstr "chvenie = 50"

#: calligraphy-f07.svg:213(tspan)
#, no-wrap
msgid "tremor = 70"
msgstr "chvenie = 70"

#: calligraphy-f07.svg:224(tspan)
#, no-wrap
msgid "tremor = 90"
msgstr "chvenie = 90"

#: calligraphy-f07.svg:235(tspan)
#, no-wrap
msgid "tremor = 20"
msgstr "chvenie = 20"

#: calligraphy-f07.svg:246(tspan)
#, no-wrap
msgid "tremor = 40"
msgstr "chvenie = 40"

#: calligraphy-f07.svg:257(tspan)
#, no-wrap
msgid "tremor = 60"
msgstr "chvenie = 60"

#: calligraphy-f07.svg:268(tspan)
#, no-wrap
msgid "tremor = 80"
msgstr "chvenie = 80"

#: calligraphy-f07.svg:279(tspan)
#, no-wrap
msgid "tremor = 100"
msgstr "chvenie = 100"

#: calligraphy-f06.svg:80(tspan) calligraphy-f06.svg:96(tspan)
#: calligraphy-f06.svg:112(tspan) calligraphy-f05.svg:66(tspan)
#, no-wrap
msgid "angle = 30"
msgstr "uhol = 30"

#: calligraphy-f06.svg:84(tspan)
#, no-wrap
msgid "fixation = 100"
msgstr "fixácia = 100"

#: calligraphy-f06.svg:100(tspan)
#, no-wrap
msgid "fixation = 80"
msgstr "fixácia = 80"

#: calligraphy-f06.svg:116(tspan)
#, no-wrap
msgid "fixation = 0"
msgstr "fixácia = 0"

#: calligraphy-f05.svg:77(tspan)
#, no-wrap
msgid "angle = 60"
msgstr "uhol = 60"

#: calligraphy-f05.svg:88(tspan)
#, no-wrap
msgid "angle = 90"
msgstr "uhol =90"

#: calligraphy-f05.svg:99(tspan) calligraphy-f04.svg:132(tspan)
#, no-wrap
msgid "angle = 0"
msgstr "uhol = 0"

#: calligraphy-f05.svg:110(tspan)
#, no-wrap
msgid "angle = 15"
msgstr "uhol = 15"

#: calligraphy-f05.svg:121(tspan)
#, no-wrap
msgid "angle = -45"
msgstr "uhol = -45"

#: calligraphy-f04.svg:110(tspan)
#, no-wrap
msgid "angle = 90 deg"
msgstr "uhol = 90 stupňov"

#: calligraphy-f04.svg:121(tspan)
#, no-wrap
msgid "angle = 30 (default)"
msgstr "uhol = 30 (predvolené)"

#: calligraphy-f04.svg:143(tspan)
#, no-wrap
msgid "angle = -90 deg"
msgstr "uhol = -90 stupňov"

#: calligraphy-f02.svg:66(tspan)
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "stenčovanie = 0 (nemenná šírka)"

#: calligraphy-f02.svg:77(tspan)
#, no-wrap
msgid "thinning = 10"
msgstr "stenčovanie = 10"

#: calligraphy-f02.svg:88(tspan)
#, no-wrap
msgid "thinning = 40"
msgstr "stenčovanie = 40"

#: calligraphy-f02.svg:99(tspan)
#, no-wrap
msgid "thinning = -20"
msgstr "stenčovanie = -20"

#: calligraphy-f02.svg:110(tspan)
#, no-wrap
msgid "thinning = -60"
msgstr "stenčovanie = -60"

#: calligraphy-f01.svg:66(tspan)
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "šírka=0.01, rastie....                       dosahuje 0.47, klesá...                                   späť na 0"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:144(None)
msgid "@@image: 'calligraphy-f01.svg'; md5=f27939e8c74b022d1d270f16b17c1484"
msgstr "@@image: 'calligraphy-f01.svg'; md5=f27939e8c74b022d1d270f16b17c1484"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:160(None)
msgid "@@image: 'calligraphy-f02.svg'; md5=0caeffd9b92114393aa383dbc325421f"
msgstr "@@image: 'calligraphy-f02.svg'; md5=0caeffd9b92114393aa383dbc325421f"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:172(None)
msgid "@@image: 'calligraphy-f03.svg'; md5=4650561753cc124def3629d1cd61836a"
msgstr "@@image: 'calligraphy-f03.svg'; md5=4650561753cc124def3629d1cd61836a"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:192(None)
msgid "@@image: 'calligraphy-f04.svg'; md5=576e94131ef9f3a278a179ec4146f39e"
msgstr "@@image: 'calligraphy-f04.svg'; md5=576e94131ef9f3a278a179ec4146f39e"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:209(None)
msgid "@@image: 'calligraphy-f05.svg'; md5=0bedc63ef08031de2b8de8bd2757665a"
msgstr "@@image: 'calligraphy-f05.svg'; md5=0bedc63ef08031de2b8de8bd2757665a"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:230(None)
msgid "@@image: 'calligraphy-f06.svg'; md5=0d0ca38000f0a14ba641cb8c22ae93f4"
msgstr "@@image: 'calligraphy-f06.svg'; md5=0d0ca38000f0a14ba641cb8c22ae93f4"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:255(None)
msgid "@@image: 'calligraphy-f07.svg'; md5=37055cfeff519d5a6ef7f0577cd53060"
msgstr "@@image: 'calligraphy-f07.svg'; md5=37055cfeff519d5a6ef7f0577cd53060"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:304(None)
msgid "@@image: 'calligraphy-f08.svg'; md5=93bbba3eaf9bfd44ad859e76d11802d1"
msgstr "@@image: 'calligraphy-f08.svg'; md5=93bbba3eaf9bfd44ad859e76d11802d1"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:322(None)
msgid "@@image: 'calligraphy-f09.svg'; md5=b3812b1642a8927f25557a33011dfcf2"
msgstr "@@image: 'calligraphy-f09.svg'; md5=b3812b1642a8927f25557a33011dfcf2"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:359(None)
msgid "@@image: 'calligraphy-f10.svg'; md5=f5de4610f6e4d340376ac847522e57b8"
msgstr "@@image: 'calligraphy-f06.svg'; md5=fc36d9da92c99a158ac2ec651a1b312b"

#: tutorial-calligraphy.xml:4(title)
msgid "Calligraphy"
msgstr "Kaligrafia"

#: tutorial-calligraphy.xml:5(author)
msgid "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
msgstr "bulia byak, buliabyak@users.sf.net a josh andler, scislac@users.sf.net"

#: tutorial-calligraphy.xml:9(para)
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Jeden z mnohých nástrojov, ktoré sú k dispozícii v programe Inkscape je aj "
"kaligrafický nástroj. Tento návod vám pomôže oboznámiť sa s tým, ako funguje "
"a naučí vás niektoré základné techniky kaligrafického umenia."

#: tutorial-calligraphy.xml:14(para)
msgid ""
"Use <keycap>Ctrl+arrows</keycap>, <keycap>mouse wheel</keycap>, or "
"<keycap>middle button drag</keycap> to scroll the page down. For basics of "
"object creation, selection, and transformation, see the Basic tutorial in "
"<command>Help &gt; Tutorials</command>."
msgstr ""
"Na posúvanie stránky použite <keycap>Ctrl+šípky</keycap>,<keycap>koliesko "
"myši</keycap> alebo <keycap>ťahanie stredným tlačidlom</keycap>. O ďalších "
"základných úkonoch, akými sú vytváranie objektov, označenie a posúvanie, sa "
"dozviete v návode Základy v ponuke  <command>Pomocník &gt; Návody</command>."

#: tutorial-calligraphy.xml:22(title)
msgid "History and Styles"
msgstr "História a štýly "

#: tutorial-calligraphy.xml:24(para)
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, calligraphy "
"is the art of making beautiful or elegant handwriting. It may sound "
"intimidating, but with a little practice, anyone can master the basics of "
"this art."
msgstr ""
"Ak pozriete v slovníku definíciu, zistíte, že <firstterm>kaligrafia</"
"firstterm> znamená „ozdobné písmo“ alebo „umelecké písmo alebo krasopis“. "
"Podstata kaligrafie spočíva v umení vytvorenia krásneho alebo elegantného "
"rukopisu. To môže znieť zastrašujúco, ale s trochou praxe môže každý zvládnuť "
"základy tohto umenia."

#: tutorial-calligraphy.xml:31(para)
msgid ""
"The earliest forms of calligraphy date back to cave-man paintings. Up until "
"roughly 1440 AD, before the printing press was around, calligraphy was the "
"way books and other publications were made. A scribe had to handwrite every "
"individual copy of every book or publication. The handwriting was done with a "
"quill and ink onto materials such as parchment or vellum. The lettering "
"styles used throughout the ages include Rustic, Carolingian, Blackletter, "
"etc. Perhaps the most common place where the average person will run across "
"calligraphy today is on wedding invitations."
msgstr ""
"Najstaršie formy kaligrafie sa datujú do doby jaskynných malieb. Až do roku "
"1440, kedy bol vynájdený tlačiarenský lis, sa kaligrafia využívala na písanie "
"kníh a ďalších publikácií. Pisári museli ručne prepísať každú kópiu všetkých "
"kníh a publikácií. Na písanie sa používalo husacie pero a atrament na "
"materiály ako je pergamen. Medzi najčastejšie používané štýly písania patria "
"štýly Rustic, Carolingian, Blackletter atď. Miesto, kde sa v súčasnosti môže "
"bežný človek najčastejšie stretnúť s kaligrafiou, sú asi svadobné oznámenia a "
"pozvánky."

#: tutorial-calligraphy.xml:41(para)
msgid "There are three main styles of calligraphy:"
msgstr "Existujú tri hlavné štýly kaligrafie: "

#: tutorial-calligraphy.xml:46(para)
msgid "Western or Roman"
msgstr "západný alebo románsky"

#: tutorial-calligraphy.xml:49(para)
msgid "Arabic"
msgstr "arabský"

#: tutorial-calligraphy.xml:52(para)
msgid "Chinese or Oriental"
msgstr "čínsky alebo orientálny"

#: tutorial-calligraphy.xml:57(para)
msgid ""
"This tutorial focuses mainly on Western calligraphy, as the other two styles "
"tend to use a brush (instead of a pen with nib), which is not how our "
"Calligraphy tool currently functions."
msgstr ""
"Tento návod je zameraný viac na západnú kaligrafiu ako na ostatné dva štýly, "
"pri ktorých sa používa štetec (miesto atramentového pera), čo nie je spôsob "
"akým náš kaligrafický nástroj v súčasnosti funguje."

#: tutorial-calligraphy.xml:63(para)
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<command>Undo</command> command: If you make a mistake, the entire page is "
"not ruined. Inkscape's Calligraphy tool also enables some techniques which "
"would not be possible with a traditional pen-and-ink."
msgstr ""
"Oproti pisárom z minulosti máme jednu obrovskú výhodu, ktorou je príkaz "
"<command>Vrátiť späť</command>: Keď urobíte chybu, nie je celá stránka "
"zničená. Kaligrafický nástroj tiež umožňuje niektoré techniky, ktoré by "
"nebolo možné použiť pri klasickom písaní atramentovým perom."

#: tutorial-calligraphy.xml:72(title)
msgid "Hardware"
msgstr "Hardvér"

#: tutorial-calligraphy.xml:74(para)
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there will "
"be some difficulty producing fast sweeping strokes."
msgstr ""
"Najlepšie výsledky dosiahnete, ak použijete <firstterm>tablet </firstterm> "
"(napr. Wacom). Vďaka flexibilite nášho nástroja môžete dokonca aj pomocou "
"myši vytvoriť jednoduchšiu kaligrafiu, no pri pokročilejšom písaní budete mať "
"problémy s vytvorením rýchlych ťahov."

#: tutorial-calligraphy.xml:81(para)
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Inkscape je schopný využiť funkcie tabletu <firstterm>citlivosť na tlak</"
"firstterm> a <firstterm>sklon pera</firstterm>. Funkcie citlivosti sú v "
"normálnom stave vypnuté, pretože je ich potrebné najskôr nastaviť. "
"Nezabudnite tiež, že kaligrafické písanie pomocou atramentového pera nie je "
"veľmi citlivé na tlak narozdiel od štetca."

#: tutorial-calligraphy.xml:88(para)
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <firstterm>Input Devices...</firstterm> dialog through the "
"<emphasis>Edit</emphasis> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar buttons "
"for pressure and tilt. From now on, Inkscape will remember those settings on "
"startup."
msgstr ""
"Ak máte tablet a chceli by ste používať funkcie citlivosti, je potrebné "
"najskôr nastaviť vaše zariadenie. Toto nastavenie stačí vykonať raz a uložiť "
"ho. Ak chcete zapnúť podporu tejto funkcie, musíte tablet pripojiť pred "
"spustením programu Inkscape a potom otvoriť dialóg <firstterm>Vstupné "
"zariadenia...</firstterm> v ponuke <emphasis>Upraviť</emphasis>. V tomto "
"dialógovom okne vyberte preferované zariadenie a nastavenia pera tabletu. Po "
"výbere týchto nastavení sa nakoniec prepnite na kaligrafický nastroj a na "
"nástrojovej lište zapnite tlačidlá tlak a sklon. Odteraz si Inkscape bude "
"tieto nastavenia pamätať. "

#: tutorial-calligraphy.xml:99(para)
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a mouse, "
"you'll probably want to zero this parameter."
msgstr ""
"Kaligrafické pero programu Inkscape môže byť citlivé na <firstterm>rýchlosť</"
"firstterm> ťahu (pozri „Stenčovanie“ nižšie), takže ak používate myš, bude "
"pravdepodobne potrebné nastaviť tento parameter na nulu."

#: tutorial-calligraphy.xml:107(title)
msgid "Calligraphy Tool Options"
msgstr "Možnosti kaligrafického nástroja"

#: tutorial-calligraphy.xml:109(para)
msgid ""
"Switch to the Calligraphy tool by pressing <keycap>Ctrl+F6</keycap>, pressing "
"the <keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: Width &amp; Thinning; Angle "
"&amp; Fixation; Caps; Tremor, Wiggle &amp; Mass. There are also two buttons "
"to toggle tablet Pressure and Tilt sensitivity on and off (for drawing "
"tablets)."
msgstr ""
"Kaligrafický nástroj zapnete stlačením <keycap>Ctrl+F6</keycap>, tiež "
"stlačením klávesy <keycap>C</keycap> alebo kliknutím na jeho ikonu na "
"nástrojovej lište. Na hornej lište si môžete všimnúť 7 volieb: Šírka a "
"Stenčovanie; Uhol a Fixácia; Zakončenie; Chvenie a Hmotnosť a odpor. Sú tu "
"tiež dve tlačidlá na zapnutie citlivosti na tlak a sklon pera tabletu."

#: tutorial-calligraphy.xml:119(title)
msgid "Width &amp; Thinning"
msgstr "Šírka a stenčovanie"

#: tutorial-calligraphy.xml:121(para)
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr ""
"Táto dvojica ovláda <firstterm>šírku</firstterm> vášho pera. Hrúbka sa môže "
"meniť od 1 do 100 a (v predvolenom nastavení) meria sa v jednotkách v pomere "
"k veľkosti okna úprav, ale nezávisle na mierke zobrazenia. Táto jednotka bola "
"zvolená, pretože fyzická „merná jednotka“ v kaligrafii je podľa rozsahu "
"pohybu vašej ruky a preto je vhodné mať hrúbku hrotu vášho pera v konštantnom "
"pomere k veľkosti „kresliacej plochy“ a nie od skutočných jednotiek, ktoré by "
"boli závislé na mierke zobrazenia. Toto správanie je voliteľné a tí, ktorí "
"chcú miesto neho používať absolútne jednotky bez ohľadu na mierku zobrazenia, "
"si ho môžu zmeniť. Ak sa chcete prepnúť do tohto režimu, použite zaškrtávacie "
"pole na stránke s nastaveniami nástroja (môžete ju otvoriť dvojitým kliknutím "
"na tlačidlo nástroja)."

#: tutorial-calligraphy.xml:134(para)
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Vzhľadom na to, že šírka sa mení často, môžete ju nastaviť aj bez toho, aby "
"ste prešli na lištu. Použite <keycap>ľavú</keycap> a <keycap>pravú</keycap> "
"šípku na klávesnici alebo hrúbku z tabletu, ktorý podporuje funkciu citlivosť "
"na tlak. Tieto šípky fungujú aj počas kreslenia, takže môžete postupne meniť "
"hrúbku uprostred ťahu."

#: tutorial-calligraphy.xml:149(para)
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"Šírka pera môže tiež závisieť od rýchlosti pohybu, ktorá mení parameter "
"<firstterm>stenčovanie</firstterm>. Tento parameter môže nadobúdať hodnoty v "
"rozsahu od -100 do 100; nula znamená, že hrúbka je nezávislá od rýchlosti "
"pohybu, kladné hodnoty rýchlejší ťah stenčujú a záporné hodnoty rýchlejší ťah "
"rozširujú. Predvolená hodnota 10 znamená mierne stenčovanie rýchlych ťahov. "
"Tu je niekoľko príkladov, ktoré boli nakreslené s hodnotami hrúbka=20 a "
"uhol=90:"

#: tutorial-calligraphy.xml:165(para)
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Len tak pre zábavu nastavte šírku a stenčovanie na 100 (maximum) a kreslite "
"trhanými pohybmi. Dostanete naturalistické útvary podobné neurónom:"

#: tutorial-calligraphy.xml:180(title)
msgid "Angle &amp; Fixation"
msgstr "Uhol a fixácia"

#: tutorial-calligraphy.xml:182(para)
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the angle "
"parameter is greyed out and the angle is determined by the tilt of the pen."
msgstr ""
"Po šírke je druhým najdôležitejším parametrom <firstterm>uhol</firstterm>. Je "
"to uhol otočenia pera v stupňoch, mení sa od 0 (horizontálna poloha) po 90 "
"(proti smeru hodinových ručičiek do vertikálnej polohy) alebo po -90 (v smere "
"hodinových ručičiek do vertikálnej polohy). Všimnite si, že ak zapnete "
"citlivosť tabletu na sklon pera, parameter uhla zošedne a  uhol bude určený "
"podľa sklonu pera tabletu."

#: tutorial-calligraphy.xml:197(para)
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands and "
"more experienced calligraphers will often vary the angle while drawing, and "
"Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Každý tradičný kaligrafický štýl používa svoj vlastný uhol otočenia pera. "
"Napríklad Unical používa uhol 25 stupňov. Zložitejšie rukopisy a zručnejší "
"kaligrafi často menia uhol počas písania, a preto to Inkscape umožňuje "
"pomocou šípok klávesnice <keycap>hore</keycap> a <keycap>dole</keycap> alebo "
"tabletu, ktorý podporuje detekciu sklonu pera. Pri prvých pokusoch s "
"kaligrafiou však radšej nechajte uhol konštantný. Tu sú príklady ťahov "
"nakreslených s rôznym uhlom (fixácia=100):"

#: tutorial-calligraphy.xml:214(para)
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Ako môžete vidieť, ťah je tenší, ak je kreslený rovnobežne s otočením pera a "
"hrubší, ak je kreslený kolmo. Kladné uhly sú prirodzenejšie pre kaligrafiu "
"písanú pravákmi."

#: tutorial-calligraphy.xml:220(para)
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"Úroveň kontrastu medzi najtenšou a najhrubšou čiarou sa riadi parametrom "
"<firstterm>fixácia</firstterm>. Hodnota 100 znamená, že uhol bude nemenný "
"tak, ako je nastavený v poli Uhol. Znížením fixácie povolíte peru trochu sa "
"pootočiť v smere ťahu. Ak nastavíte fixácia=0, pero sa bude plne "
"prispôsobovať smeru ťahu a nastavenie uhla nebude mať žiadny vplyv:"

#: tutorial-calligraphy.xml:235(para)
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke width "
"contrast (above left) are the features of antique serif typefaces, such as "
"Times or Bodoni (because these typefaces were historically an imitation of "
"fixed-pen calligraphy). Zero fixation and zero width contrast (above right), "
"on the other hand, suggest modern sans serif typefaces such as Helvetica."
msgstr ""
"V typografickej reči maximálna fixácia, teda maximálny kontrast šírky (hore "
"vľavo), predstavuje antický rukopis ako napríklad Times alebo Bodoni (pretože "
"ich vzhľad je napodobeninou historických kaligrafov). Nulová fixácia, teda "
"nulový kontrast (hore vpravo), na druhej strane pripomína moderný rukopis ako "
"napríklad Helvetica."

#: tutorial-calligraphy.xml:245(title)
msgid "Tremor"
msgstr "Chvenie"

#: tutorial-calligraphy.xml:247(para)
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"<firstterm>Chvenie</firstterm> slúži na to, aby dodalo kaligrafickým ťahom "
"realistickejší vzhľad. Chvenie sa dá nastaviť v hornej lište pomocou hodnôt v "
"rozsahu od 0 do 100. Prejaviť sa môže od vytvorenia jemných nerovností až po "
"vytvorenie divokých škvŕn a machúľ, čo podstatne rozširuje tvorivý rozsah "
"tohto nástroja."

#: tutorial-calligraphy.xml:262(title)
msgid "Wiggle &amp; Mass"
msgstr "Chvenie a hmotnosť"

#: tutorial-calligraphy.xml:264(para)
msgid ""
"Unlike width and angle, these two last parameters define how the tool “feels” "
"rather than affect its visual output. So there won't be any illustrations in "
"this section; instead just try them yourself to get a better idea."
msgstr ""
"Na rozdiel od šírky a uhla tieto dva posledné parametre definujú „pocit“ z "
"nástroja miesto vzhľadu, preto sa nedajú v tomto návode ilustrovať. Aby ste o "
"nich získali predstavu, bude najlepšie, ak si ich sami vyskúšate."

#: tutorial-calligraphy.xml:270(para)
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"<firstterm>Odpor</firstterm> predstavuje odpor papiera, ktorý kladie pri "
"pohybe pera. Predvolené nastavenie je na minimum (0) a zníženie tohto "
"parametra spôsobí to, že papier bude „klzký“: v prípade, že je hmotnosť pera "
"veľká, pero má tendenciu utekať do ostrých zákrut, keď je hmotnosť nulová, "
"začne sa sa pero aj pri malých pohyboch krútiť."

#: tutorial-calligraphy.xml:277(para)
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your mouse "
"pointer and the more it smoothes out sharp turns and quick jerks in your "
"stroke. By default this value is quite small (2) so that the tool is fast and "
"responsive, but you can increase mass to get slower and smoother pen."
msgstr ""
"Vo fyzike je <firstterm>hmotnosť</firstterm> to, čo spôsobuje zotrvačnosť. "
"Väčšia hmotnosť pri kaligrafickom nástroji programu Inkscape spôsobí väčšie "
"oneskorenie za kurzorom myši a tým sa v ťahu vyhladzujú ostré prudké zmeny "
"pohybu a trhnutia. Predvolená hodnota tohto nastavenia je malá (2), preto je "
"nástroj rýchly a reaktívny. Ak chcete pero spomaliť a dosiahnuť plynulejšie "
"ťahy, zvýšte túto hodnotu."

#: tutorial-calligraphy.xml:288(title)
msgid "Calligraphy examples"
msgstr "Príklady kaligrafie"

#: tutorial-calligraphy.xml:290(para)
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Teraz, keď už poznáte základné možnosti nástroja, môžete sa pokúsiť vytvoriť "
"naozajstnú kaligrafiu. Ak toto umenie ešte nepoznáte, vezmite si nejakú dobrú "
"knihu a študujte ju spolu s programom Inkscape. V tejto časti si ukážeme "
"niekoľko jednoduchých príkladov."

#: tutorial-calligraphy.xml:296(para)
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Predtým, než začnete kresliť písmená, vytvorte si dve vodiace čiary. Ak "
"chcete písať so sklonom, vytvorte si dve šikmé vodiace čiary, napríklad takto:"

#: tutorial-calligraphy.xml:309(para)
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Potom zvoľte mierku zobrazenia tak, aby vodiace čiary čo najviac zodpovedali "
"vášmu prirodzenému pohybu ruky, nastavte šírku a uhol a môžte ísť na to!"

#: tutorial-calligraphy.xml:314(para)
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round strokes, "
"slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"Pravdepodobne prvou vecou, ktorú by ste sa ako začínajúci kaligrafi mali "
"naučiť, je písanie základných prvkov písmen — vodorovných, zvislých, "
"zaoblených a šikmých ťahov. Tu je niekoľko prvkov písmen z rukopisu Unicial:"

#: tutorial-calligraphy.xml:327(para)
msgid "Several useful tips:"
msgstr "Niekoľko užitočných rád:"

#: tutorial-calligraphy.xml:332(para)
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll the "
"canvas (<keycap>Ctrl+arrow</keycap> keys) with your left hand after finishing "
"each letter."
msgstr ""
"Ak máte ruku pohodlne položenú na tablete, nehýbte ňou. Miesto toho po "
"dokončení písmena radšej posuňte plátno (<keycap>Ctrl+šípka</keycap>) vašou "
"ľavou rukou."

#: tutorial-calligraphy.xml:337(para)
msgid ""
"If your last stroke is bad, just undo it (<keycap>Ctrl+Z</keycap>). However, "
"if its shape is good but the position or size are slightly off, it's better "
"to switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using mouse or keys), then press <keycap>Space</keycap> "
"again to return to Calligraphy tool."
msgstr ""
"Ak je váš posledný ťah zlý, tak ho vráťte (<keycap>Ctrl+Z</keycap>). Keď je "
"tvar správny, ale umiestnenie alebo veľkosť je trochu odlišná, je dobré "
"prepnúť na nástroj Výber (<keycap>medzera</keycap>) a posunúť, natiahnuť "
"alebo otočiť útvar podľa potreby (pomocou myši alebo klávesnice). Potom znova "
"stlačte <keycap>medzeru</keycap>, ktorá opäť zapne kaligrafický nástroj."

#: tutorial-calligraphy.xml:345(para)
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Po dokončení slova sa prepnite opäť na nástroj Výber, nastavte pravidelnosť "
"paličiek a rozostup písmen. Nepreháňajte to však. Dobrá kaligrafia musí "
"vyzerať ako niečo napísané rukou. Vyhnite sa kopírovaniu písmen a ich "
"jednotlivých prvkov. Každý ťah musí byť jedinečný."

#: tutorial-calligraphy.xml:353(para)
msgid "And here are some complete lettering examples:"
msgstr "Tu je niekoľko ukážok hotových písmen:"

#: tutorial-calligraphy.xml:369(title)
msgid "Conclusion"
msgstr "Zhrnutie"

#: tutorial-calligraphy.xml:371(para)
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with and "
"may be useful in real design. Enjoy!"
msgstr ""
"Kaligrafia nie je len zábava, je to hlboko duchovné umenie, ktoré môže zmeniť "
"váš pohľad na všetko, čo robíte a vidíte. Kaligrafický nástroj programu "
"Inkscape môže slúžiť len ako skromný úvod. Aj napriek tomu je veľmi príjemný "
"na hranie a môže byť užitočný aj v reálnom prevedení. Užite si ho!"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-calligraphy.xml:0(None)
msgid "translator-credits"
msgstr ""
"Peter Mráz <etkinator@gmail.com>, 2009.\n"
"Ivan Masár <helix84@centrum.sk>, 2009, 2010, 2015."

#~ msgid "@@image: 'calligraphy-f10.svg'; md5=d9b02c7d1ff70321c44e1e1281093253"
#~ msgstr "@@image: 'calligraphy-f10.svg'; md5=d9b02c7d1ff70321c44e1e1281093253"

#, fuzzy
#~ msgid "@@image: 'calligraphy-f04.svg'; md5=6f5e90597790d5c5c8e48557a74878e8"
#~ msgstr "@@image: 'calligraphy-f10.svg'; md5=f5bab3f6024e2c07e57a166c7463ce4a"

#, fuzzy
#~ msgid "@@image: 'calligraphy-f07.svg'; md5=b0d02ae9d1284efc86763d03bc74708b"
#~ msgstr "@@image: 'calligraphy-f07.svg'; md5=700b627649326496949f861e39f37b03"

#~ msgid "@@image: 'calligraphy-f04.svg'; md5=a17232359635d30c6130782839d8231d"
#~ msgstr "@@image: 'calligraphy-f04.svg'; md5=a17232359635d30c6130782839d8231d"

#~ msgid "@@image: 'calligraphy-f02.svg'; md5=3e07b9a5a4f30560c8716d098473f1ab"
#~ msgstr "@@image: 'calligraphy-f02.svg'; md5=3e07b9a5a4f30560c8716d098473f1ab"

#~ msgid "Mass &amp; Drag"
#~ msgstr "Hmotnosť a odpor"

#~ msgid "thinning = 0.4"
#~ msgstr "stenčovanie = 40"

#~ msgid "thinning = -0.6"
#~ msgstr "stenčovanie = -60"

#~ msgid "fixation = 0.8"
#~ msgstr "fixácia = 80"

#~ msgid "tremor = 0.1"
#~ msgstr "chvenie = 10"

#~ msgid "tremor = 0.3"
#~ msgstr "chvenie = 30"

#~ msgid "tremor = 0.5"
#~ msgstr "chvenie = 50"

#~ msgid "tremor = 0.7"
#~ msgstr "chvenie = 70"

#~ msgid "tremor = 0.9"
#~ msgstr "chvenie = 90"

#~ msgid "tremor = 0.2"
#~ msgstr "chvenie = 20"

#~ msgid "tremor = 0.4"
#~ msgstr "chvenie = 40"

#~ msgid "tremor = 0.6"
#~ msgstr "chvenie = 60"

#~ msgid "tremor = 0.8"
#~ msgstr "chvenie = 80"
